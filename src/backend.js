import axios from 'axios'

export default class Backend
{
		constructor(){

		}
		static host="http://geogramma-test.byllmcsony.ru"
		static apiURL="http://geogramma-test.byllmcsony.ru/api/v1";
		static currentToken="0cL54z1gRwHwvwQsYsWgfQz5lURQMzuR";
		static currentUser=null;

		static deleteTask(task, oncomplit)
		{
			oncomplit();
		}

		static changeState(task, state, oncomplit)
		{
			oncomplit();
		}

		static updateTask(task, oncomplit)
		{
			oncomplit();
		}

		static getUser(id, oncomplit)
		{
			let url=this.apiURL+"/user/"+id;
			let ax=axios.create({baseURL:url});
			ax.defaults.headers.common['token'] = this.currentToken;
			ax.get(url).
				then( (response)=> { oncomplit(response.data) } ).
				catch( (error) => console.log(error));							
		}

		
		static getTask(id, oncomplit)
		{
			let url=this.apiURL+"/task/"+id;
			let ax=axios.create({baseURL:url});
			ax.defaults.headers.common['token'] = this.currentToken;
			ax.get(url).
				then( (response)=> { oncomplit(response.data) } ).
				catch( (error) => console.log(error));							
		}

		static getTaskList(oncomplit)
		{

			let url=this.apiURL+"/tasks/";
			let ax=axios.create({baseURL:url});
			ax.defaults.headers.common['token'] = this.currentToken;
			ax.get(url).
				then( (response)=> { oncomplit(response.data.data) } ).
				catch( (error) => console.log(error));				
		}


		static getUserByToken(token, oncomplit)
		{
			if(this.currentUser)
			{
				oncomplit(this.currentUser);
				return;
			}

			let url=this.apiURL+"/user/?token="+this.currentToken;
		
			axios.get(url).then( (response)=>{
					this.currentUser=response.data;
					localStorage.setItem("token", this.currentToken);
					if(oncomplit)
						oncomplit(this.currentUser);
				}).
				catch((error) =>console.log(error));
		}


		static getUserByLogin(login, password, oncomplit)
		{
			this.getUserByToken(this.currentToken, oncomplit);
			return;

			let data={"login":login, "pass":password};

			let url=this.apiURL+"/login/"

			axios.post(url, data).then( (response)=>{
				this.currentUser=response.data;
				oncomplit(this.currentUser)
			}).
			catch((error) =>console.log(error));	
		}
}