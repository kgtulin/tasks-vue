import Vue from 'vue'
import Router from 'vue-router'

import Auth from "@/components/Auth"
import Profile from "@/components/Profile"
import Task from "@/components/Task"
import TaskEdit from "@/components/TaskEdit"
import TaskList from "@/components/TaskList"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Auth',
      component: Auth
	},
	
    {
      path: '/user/:id',
      name: 'Profile',
      component: Profile
	},
	{
		path: '/profile',
		name: 'CurrentProfile',
		component: Profile
	},  
    {
		path: '/tasklist',
		name: 'TaskList',
		component: TaskList
	},
	  
    {
		path: '/tasklist/:id',
		name: 'Task',
		component: Task
	},

    {
		path: '/tasklist/:id/edit',
		name: 'TaskEdit',
		component: TaskEdit
	},
	  
  ]
})
